#April tag processing

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pymvg.calibration 
import pymvg.multi_camera_system
import imageio
import scipy.optimize



#Define coordinates 
# TODO make this in another format? from sheet generator in matlab??
Z1_METERS = 0.15

def id2xyz(idnum):
    
    # These values are the X and Y positions of the A4 sheets as defined in the Inkscape
    # `drawing.svg` file and rendered in the `Z0.pdf` and `Z1.pdf` file. All units are meters.
    
    x0a = -0.0935
    x1a = 0.08777
    
    x0b = -0.0838
    x1b = 0.0833
    
    y0 = -0.0938
    y1 = 0.0872
    
    za = 0.0
    zb = Z1_METERS

    rownum = idnum // 45
    colnum = idnum % 45
    
    if colnum >= 15:
        # sheet z1
        colnum -= 15
        x0 = x0b
        x1 = x1b        
        z = zb
    else:
        # sheet z0
        x0 = x0a
        x1 = x1a
        z = za
    
    xrange = x1-x0
    xnum = 13
    dx = xrange/xnum

    yrange = y1-y0
    ynum = 14
    dy = yrange/ynum
    
    x = colnum * dx + x0
    y = rownum * dy + y0
    return (x, y, z)
    
def id2xyz2(idnum):
    
    # These values are the X and Y positions of the A4 sheets as defined in the Inkscape
    # `drawing.svg` file and rendered in the `Z0.pdf` and `Z1.pdf` file. All units are meters.
    
    x0a = -0.0935
    x1a = 0.08777
    
    x0b = -0.0838
    x1b = 0.0833
    
    y0 = -0.0938
    y1 = 0.0872
    
    z0 = 0.0
    z1 = Z1_METERS

    rownum = idnum // 45
    colnum = idnum % 45
    
    if colnum >= 15:
        # sheet z1
        colnum -= 15
        x0 = x0b
        x1 = x1b        
        z = z1
    else:
        # sheet z0
        x0 = x0a
        x1 = x1a
        z = z0
    
    xrange = x1-x0
    xnum = 13
    dx = xrange/xnum

    yrange = y1-y0
    ynum = 14
    dy = yrange/ynum
    
    x = colnum * dx + x0
    y = rownum * dy + y0
    return (x, y, z)

class DistortionOptimizer:
    def __init__(self, X3d, x2d, linear_cam):
        self.X3d = X3d
        self.x2d = x2d
        self.linear_cam = linear_cam
    def mean_reproj_err(self, d):
        """Find the mean reprojection error for d, the distortion parameters

        Arguments
        ---------
        d - a length 4 sequence [radial1, radial2, tangential1, tangential2]

        Notes
        -----
        Although pymvg uses a length-5 distortion vector (radial1, radial2,
        tangential1, tangential2, radial3) to fit the ROS/OpenCV plumb_bob
        distortion model, the Flydra XML calibration format does not use the
        radial3 distortion term. Therefore, we optimize the distortion while
        keeping radial3 fixed to zero.
        """
        d = np.array([d[0], d[1], d[2], d[3], 0.0], dtype=float)
        cam = self.linear_cam
        cam.distortion = d
        x2d_reproj = cam.project_3d_to_pixel(self.X3d)

        # calculate point-by-point reprojection error
        err = np.sqrt(np.sum( (self.x2d - x2d_reproj)**2, axis=1 ))

        # find mean reprojection error across all pixels
        mean_reproj_err = np.mean(err)
        return mean_reproj_err
if __name__ == "__main__":

    cam_height = 1088 #in pixels
    cam_width = 2048    

    df_low = pd.read_csv('z0.csv',skiprows=8)
    df_high = pd.read_csv('z1.csv',skiprows=8)

    df = pd.concat((df_low,df_low,df_high))

    # Create "human" names for x and y center.
    df['x_px'] = df['h02']
    df['y_px'] = df['h12']

    # Average all data for the same tag.
    df = df.groupby(['id'], as_index=False).mean()
    # Compute know 3D location of each tag
    df['x'],df['y'],df['z'] = zip(*df['id'].map(id2xyz))

    # World coords in 3D
    X3d = np.array((df['x'].values, df['y'].values, df['z'].values)).T
    # Pixel coords in 2D
    x2d = np.array((df['x_px'].values, df['y_px'].values)).T
    #Run the calibration
    dlt_results = pymvg.calibration.DLT(X3d, x2d, width=cam_width, height=cam_height)

    #Print results from DLT
    for key in dlt_results:
        print(key, dlt_results[key])

    #Rename
    cam1 = dlt_results['cam']
    cam1.name = 'cam1'

    optimizer = DistortionOptimizer(X3d, x2d, cam1)

    optimizer_results = scipy.optimize.minimize(optimizer.mean_reproj_err, np.zeros(4))
    print(optimizer_results)

    d = optimizer_results.x

    # Now update the distortion model with our new parameters.
    cam1.distortion = np.array([d[0], d[1], d[2], d[3], 0.0], dtype=float)

    cameras = [cam1]
    cam_system = pymvg.multi_camera_system.MultiCameraSystem(cameras)
    cam_system.save_to_pymvg_file("calibration_pymvg.json")