#Offline retrack braidz files

# braid-offline-retrack 0.12.0-alpha.2

# USAGE:
#     braid-offline-retrack [OPTIONS] -d <data-src> -o <output>

# FLAGS:
#     -h, --help       Prints help information
#     -V, --version    Prints version information

# OPTIONS:
#     -d <data-src>                              Input .braidz file
#         --fps <fps>                            Set frames per second
#     -o <output>                                Output file (must end with .braidz)
#         --start-frame <start-frame>            Set start frame to start tracking
#         --stop-frame <stop-frame>              Set stop frame to stop tracking
#         --tracking-params <tracking-params>    Tracking parameters TOML file

import os
import subprocess
import shutil
from zipfile import ZipFile 

#ToDo
#To make it nicer
#Braidz file indexing
#Batch processing
#Default output name
#GUI
#parser input

#Test calibration toml-file OR convert toml -> xml

if __name__ == "__main__":

    braidz_path = '/home/reken001/offline-retrack'
    braidz_filename = 'test.braidz'

    braidz_output_path = '/home/reken001/offline-retrack'
    braidz_output_filename = 'retracked-test.braidz'

    update_calibration = False 
    calibration_path = '/home/reken001/offline-retrack'
    calibration_filename = 'calib.xml' #xml is required

    update_tracking_parameters = True
    tracking_parameters_path = '/home/reken001/offline-retrack'
    tracking_parameters_file = 'param.toml'

    input_braidz = os.path.join(braidz_path, braidz_filename)
    output_braidz = os.path.join(braidz_output_path, braidz_output_filename)
    tracking_param = os.path.join(tracking_parameters_path, tracking_parameters_file)

    if update_calibration:
        # writing files to a zipfile
        # rename calibration file to calibration.xml
        src = os.path.join(calibration_path, calibration_filename)
        dst = os.path.join(calibration_path, 'calibration.xml')
        shutil.copy(src,dst)
        with ZipFile(input_braidz,'a') as zip:
            zip.write(dst)

        os.remove(dst)

    if update_tracking_parameters:
        try:
            braid = subprocess.run(
            ["braid-offline-retrack -d " + input_braidz + " -o " + output_braidz 
                + " --tracking-params " + tracking_param], 
                shell=True, timeout=10, capture_output=True,text=True)
            print(braid.stdout)     
        except FileNotFoundError as exc:
            print(f"Process failed because the executable could not be found.\n{exc}")
        except subprocess.CalledProcessError as exc:
            print(
                f"Process failed because did not return a successful return code. "
                f"Returned {exc.returncode}\n{exc}"
            )
        except subprocess.TimeoutExpired as exc:
            print(f"Process timed out.\n{exc}")
    else:
        try:
            braid = subprocess.run(["braid-offline-retrack -d " + input_braidz + " -o " + output_braidz], 
                shell=True, timeout=10, capture_output=True,text=True)
            print(braid.stdout)     
        except FileNotFoundError as exc:
            print(f"Process failed because the executable could not be found.\n{exc}")
        except subprocess.CalledProcessError as exc:
            print(
                f"Process failed because did not return a successful return code. "
                f"Returned {exc.returncode}\n{exc}"
                )
        except subprocess.TimeoutExpired as exc:
            print(f"Process timed out.\n{exc}")
