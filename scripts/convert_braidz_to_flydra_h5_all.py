import os, subprocess


if __name__ == "__main__":

    braidz_path = '/home/reken001/BRAID/Tracking'

    start_date = '20211005'
    end_date = '20211105'
    start_time = 0  # hour + min (720 = 7:20)
    end_time = 2400

    last_day = str(int(start_date) - 1)
    cp_per_day = 1

    all_tracking_filename = sorted(os.listdir(braidz_path), key=lambda x: x[0:15])

    for filename in all_tracking_filename:
        if not filename[-1] == '5' and (filename + '.h5') not in all_tracking_filename \
                and int(start_date) <= int(filename[0:8]) <= int(end_date) and start_time <= int(filename[9:13]) < end_time:

            if filename[0:8] == last_day:
                cp_per_day = cp_per_day + 1

            else:
                cp_per_day = 1
                last_day = filename[0:8]

            print(filename + ': Converting...')
            try:
                subprocess.call(['python',
                    os.path.join(os.path.dirname(__file__), 'convert_kalmanized_csv_to_flydra_h5.py'),
                    os.path.join(braidz_path, filename)])

            except ValueError as e:
                print('ValueError!! {0}'.format(e))

            print(filename + ': Finished converting!')
