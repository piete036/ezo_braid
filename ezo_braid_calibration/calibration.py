# Braid calibration workflow EZO
# RP jun 2022

# Calibration method. MCSC + AprilTags or only AprilTags
CALIB_METHOD = "MCSC" #MCSC or AprilTags

# Data directories
CALIBRATION_DIRNAME = "/home/reken001/BRAID/Calibration" # Directory with camera system calibration
MCSC_DIRNAME = CALIBRATION_DIRNAME + "/MCSC" # Directory with MCSC input (braidz-file) and output
APRILTAG_DIRNAME = CALIBRATION_DIRNAME + "/AprilTag" # Directory with AprilTag output
CONFIG_DIRNAME = "/home/reken001/BRAID/Config" # Directory with tracking configuration files 

# Settings
APRILTAG_FNAME = "checker.toml" # Filename toml-file with AprilTag positions
NCAMS = 3 # Number of cameras in system

from fileinput import fileno
from genericpath import exists
import glob
import os
import toml
from pybraid import convert
import pymvg.multi_camera_system
import pymvg.calibration 
from pymvg.plot_utils import plot_camera
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd
import scipy.optimize
from csv import reader
import gzip

from strand_braid_user.analysis.strand_braid_user_analysis import plot_data2d_timeseries

def last_file(path, suffix, n = 1):
    files = glob.glob(os.path.join(path, suffix))
    if n == 1:
        last_file = max(files) # sort on timestamp in filename
    else:
        sorted_files = sorted(files, key=lambda timestamp: timestamp[-23:-8])
        if len(sorted_files) <= n:
            last_file = sorted_files[0:n]
    return last_file

def unique_filename(path, filename, suffix):    
    filenum=1
    if os.path.exists(os.path.join(path,(filename+suffix))) is False:
        file = filename+suffix
    else:
        while os.path.exists(os.path.join(path,(filename+'_'+str(filenum)+suffix))):
            filenum+=1
        file = filename+'_'+str(filenum)+suffix
    return file

def plot_camera_system(system, z = 1, title = 'Camera system', tags = None):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    Ncam = 0
    for name in system.get_names():
        plot_camera( ax, system.get_camera(name), scale = z/1.0 )
        Ncam += 1

    if tags == None:
        # put some points to force mpl's view dimensions
        pts = np.array([[0,0,0],
                        [2*z, 2*z, 2*z]])
        ax.plot( pts[:,0], pts[:,1], pts[:,2], 'k.')
    else:
        for cam in range(Ncam):
            pts = tags[cam]
            ax.plot( pts[:,0], pts[:,1], pts[:,2], 'k.')            


    ax.set_title(title)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()

def id_xyz_from_toml(path, filename, id):
    # Get tag positions from toml-file and return result in m
    data = toml.load(os.path.join(path, filename))
    unit = data['april-tag'][id]['unit']
    if unit == 'mm':
        scale = 1000
    elif unit == 'cm':
        scale = 100
    elif unit == 'm':
        scale = 1
    else:
        scale = 1
        print("AprilTag scale unkown...")

    x = data['april-tag'][id]['x']/scale
    y = data['april-tag'][id]['y']/scale
    z = data['april-tag'][id]['z']/scale
    return(x, y, z)

def apriltag_read_header(file):

    with gzip.open(file, mode = 'rt') as csv_obj:
        csv_reader = reader(csv_obj)
        header_line = 0
        for row in csv_reader:
            line = row[0] # Column A
            header_line += 1
            if "camera_name:" in line:
                txt = line.split(':')
                cam_name = txt[1].strip()
            elif "camera_width_pixels:" in line:
                txt = line.split(':')
                cam_width = int(txt[1].strip())
            elif "camera_height_pixels:" in line:
                txt = line.split(':')
                cam_height = int(txt[1].strip())
            elif "frame" in line:
                break
    apriltag = {
        "cam_name": cam_name,
        "cam_width": cam_width,
        "cam_height": cam_height,
        "header_lines": header_line -1
    }
    return apriltag

def apriltag_get_detected_ids(csv,header_lines):
    #Read detected apriltag ids in Pandas dataframe
    df= pd.read_csv(csv,compression = 'gzip', skiprows=header_lines)
    # Create "human" names for x and y center.
    df['x_px'] = df['h02']
    df['y_px'] = df['h12']
    # Average all data for the same tag.
    df = df.groupby(['id'], as_index=False).mean()
    return(df)

def apriltag_read_header_tester(cam):
    if cam == 0:
            apriltag = {
            "cam_name": 'Basler_23688799',
            "cam_width": 1024,
            "cam_height": 544,
            "header_lines": 10
        }
    if cam == 1:
            apriltag = {
            "cam_name": 'Basler_23688800',
            "cam_width": 1024,
            "cam_height": 544,
            "header_lines": 10
        }
    if cam == 2:
            apriltag = {
            "cam_name": 'Basler_23688801',
            "cam_width": 1024,
            "cam_height": 544,
            "header_lines": 10
        }
    return apriltag

def apriltag_get_detected_ids_tester(cam):
    if cam == 0:
        df_low = pd.read_csv('/home/reken001/B799_Z0_0mm.csv.gz',compression='gzip',skiprows=10)
        df_high = pd.read_csv('/home/reken001/B799_Z1_340mm.csv.gz',compression='gzip',skiprows=10)
    if cam == 1:
        df_low = pd.read_csv('/home/reken001/B800_Z0_0mm.csv.gz',compression='gzip',skiprows=10)
        df_high = pd.read_csv('/home/reken001/B800_Z1_340mm.csv.gz',compression='gzip',skiprows=10)
    if cam == 2:
        df_low = pd.read_csv('/home/reken001/B801_Z0_0mm.csv.gz',compression='gzip',skiprows=10)
        df_high = pd.read_csv('/home/reken001/B801_Z1_340mm.csv.gz',compression='gzip',skiprows=10)

    df= pd.concat((df_low,df_high))
    df['x_px'] = df['h02']
    df['y_px'] = df['h12']
    # Average all data for the same tag.
    df = df.groupby(['id'], as_index=False).mean()
    return(df)

def apriltag_get_positions(path, file, df):
    realworld = {}
    for k in df['id']:
        (x,y,z) = id_xyz_from_toml(path,file,'id'+str(k))
        if not realworld:
            realworld={'x': [x],'y': [y],'z':[z]}
        else:
            realworld['x'].append(x)
            realworld['y'].append(y)
            realworld['z'].append(z)
    
    df['x'] = realworld['x']
    df['y'] = realworld['y']
    df['z'] = realworld['z']
    return df

def apriltag_world_coord(df):
    # World coords in 3D
    X3d = np.array((df['x'].values, df['y'].values, df['z'].values)).T
    return X3d

def apriltag_pixel_coord(df):
    # Pixel coords in 2D
    x2d = np.array((df['x_px'].values, df['y_px'].values)).T
    return x2d 

def do_dlt(world_coord, pixel_coord, apriltag_header):
    dlt_result = pymvg.calibration.DLT(world_coord, pixel_coord, width=apriltag_header['cam_width'], height=apriltag_header['cam_height'])
    dlt_result['cam'].name=apriltag_header['cam_name']
    optimizer = DistortionOptimizer(world_coord, pixel_coord, dlt_result['cam'])
    optimizer_results = scipy.optimize.minimize(optimizer.mean_reproj_err, np.zeros(4))
    d = optimizer_results.x
    dlt_result['cam'].distortion = np.array([d[0], d[1], d[2], d[3], 0.0], dtype=float)
    cam = dlt_result['cam']
    return cam

class DistortionOptimizer:
    def __init__(self, X3d, x2d, linear_cam):
        self.X3d = X3d
        self.x2d = x2d
        self.linear_cam = linear_cam
    def mean_reproj_err(self, d):
            """Find the mean reprojection error for d, the distortion parameters

            Arguments
            ---------
            d - a length 4 sequence [radial1, radial2, tangential1, tangential2]

            Notes
            -----
            Although pymvg uses a length-5 distortion vector (radial1, radial2,
            tangential1, tangential2, radial3) to fit the ROS/OpenCV plumb_bob
            distortion model, the Flydra XML calibration format does not use the
            radial3 distortion term. Therefore, we optimize the distortion while
            keeping radial3 fixed to zero.
            """
            d = np.array([d[0], d[1], d[2], d[3], 0.0], dtype=float)
            cam = self.linear_cam
            cam.distortion = d
            x2d_reproj = cam.project_3d_to_pixel(self.X3d)

            # calculate point-by-point reprojection error
            err = np.sqrt(np.sum( (self.x2d - x2d_reproj)**2, axis=1 ))

            # find mean reprojection error across all pixels
            mean_reproj_err = np.mean(err)
            return mean_reproj_err


if __name__ == "__main__":

    ## AprilTag
    ## 1 Match AprilTag ID and XYZ, perform DLT
    #apriltag_csv = last_file(APRILTAG_DIRNAME,'*.csv.gz', NCAMS)
    apriltag_header = dict()
    apriltag_ids = dict()
    apriltag_pos = dict()
    world_coord = dict()
    pixel_coord = dict()
    dlt_results = dict()
    for cam in range(NCAMS):
        #apriltag_header[cam] = apriltag_read_header(apriltag_csv[cam])
        apriltag_header[cam] = apriltag_read_header_tester(cam)
        #apriltag_ids[cam] = apriltag_get_detected_ids(apriltag_csv[cam],apriltag_header[cam]['header_lines'])
        apriltag_ids[cam] = apriltag_get_detected_ids_tester(cam)
        apriltag_pos[cam] = apriltag_get_positions(CONFIG_DIRNAME,APRILTAG_FNAME,apriltag_ids[cam])
        world_coord[cam] = apriltag_world_coord(apriltag_pos[cam])
        pixel_coord[cam] = apriltag_pixel_coord(apriltag_pos[cam])
        dlt_results[cam] = do_dlt(world_coord[cam],pixel_coord[cam],apriltag_header[cam])
    
    ## 2 Calibrate system & save as JSON-file
    cameras = dlt_results.values()
    april_system = pymvg.multi_camera_system.MultiCameraSystem(cameras)
    #apriltag_fname, ledtrack_ext = os.path.splitext(os.path.basename(apriltag_csv[-1]))
    #apriltag_timestamp = apriltag_fname[-19:-4]
    #april_system.save_to_pymvg_file(os.path.join(APRILTAG_DIRNAME,apriltag_timestamp+"_calib.json"))

    ## 6 Plot the AprilTag results
    plot_camera_system(april_system, title = 'AprilTag result', tags = world_coord)
    
    if CALIB_METHOD == "MCSC":
        ## MCSC
        ## 1 Get Braidz-file with LED track & plot
        ledtrack = last_file(MCSC_DIRNAME,'*.braidz')
        print("Run MCSC on: ", ledtrack)
        plot_data2d_timeseries(ledtrack)

        ## 2 Convert Braidz in h5 & run MCSC
        convert.loadBraidz(ledtrack) #MCSC output in same dir

        ## 3 Convert MCSC to JSON
        ledtrack_fname, ledtrack_ext = os.path.splitext(os.path.basename(ledtrack))
        mcsc_results = last_file(MCSC_DIRNAME,'*.h5')+'.recal/result'

        ## 4 Read MCSC results in pyMVG & and save as JSON-file
        mcsc_system = pymvg.multi_camera_system.MultiCameraSystem.from_mcsc(mcsc_results)
        mcsc_system.save_to_pymvg_file(os.path.join(MCSC_DIRNAME,ledtrack_fname+"_calib.json"))
        mcsc_json_unaligned = last_file(MCSC_DIRNAME,'*.json')
        print("MCSC unaligned result saved as: ",mcsc_json_unaligned)

        ## 5 Plot the MCSC results
        plot_camera_system(mcsc_system, title = 'MCSC result unaligned')

        # 6 Align MCSC vs AprilTag
        april_system.get_aligned_copy(mcsc_system)
        april_system.save_to_pymvg_file(os.path.join(CALIBRATION_DIRNAME,unique_filename(CALIBRATION_DIRNAME,apriltag_timestamp+'_calib','.json')))